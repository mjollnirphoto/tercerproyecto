import './Entrie.css';

const Entrie = ({ entrie }) => {
  return (
    <li className="entrie">
      <header>
        <p>@{entrie.user || 'manolete'}</p>
        <time dateTime={entrie.createdAt}>{entrie.createdAt}</time>
      </header>
      <div>
        <p>{entrie.text}</p>
      </div>
      <footer>Botones</footer>
    </li>
  );
};

export default Entrie;
