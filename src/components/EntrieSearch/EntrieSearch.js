import './EntrieSearch.css';
import { useEffect, useState } from 'react';
import Entrie from '../Entrie/Entrie';

const EntrieSearch = () => {
  const [Entries, setEntries] = useState();
  const [keyword, setKeyword] = useState(' ');
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    const fetchData = async () => {
      try {
        const res = await fetch('http://localhost:4000/entries');
        const body = await res.json();

        if (body.status === 'error') {
          alert(body.message);
        } else {
          setEntries(body.data.Entries);
        }
      } catch (err) {
        console.error(err);
      } finally {
        setLoading(false);
      }
    };

    fetchData();
  }, []);

  //envio formulario
  const handleSubmit = async (e) => {
    e.preventDefault();

    setLoading(true);

    try {
      const res = await fetch(
        `http://localhost:4000/entries?keyword=${keyword}`
      );
      const body = await res.json();

      if (body.status === 'error') {
        alert(body.message);
      } else {
        setEntries(body.data.Entries);
      }
    } catch (err) {
      console.error(err);
    } finally {
      setLoading(false);
    }
  };

  return (
    <main className="entrie-search">
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          value={keyword}
          onChange={(e) => setKeyword(e.target.value)}
        />
        <button disabled={loading}>👀 Buscar 🔍</button>
      </form>

      <ul className="entrie-list">
        {Entries &&
          Entries.map((entrie) => {
            return <Entrie entrie={entrie} key={entrie.id} />;
          })}
      </ul>
    </main>
  );
};

export default EntrieSearch;
