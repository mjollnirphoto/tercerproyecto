import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useToken } from '../../TokenContext';
import './EntrieCreate.css';

const EntrieCreate = () => {
  const [token /*setToken*/] = useToken();
  const navigate = useNavigate();

  const [text, setText] = useState('');
  const [loading, setLoading] = useState(false);

  const handleSubmit = async (e) => {
    e.preventDefault();

    setLoading(true);

    try {
      const formData = new FormData();
      formData.append('text', text);

      const res = await fetch('http://localhost:4000/entries', {
        method: 'POST',
        headers: {
          Authorization: token,
        },
        body: formData,
      });
      const body = await res.json();

      if (body.status === 'error') {
        alert(body.message);
      } else {
        navigate('/');
      }
    } catch (err) {
      console.error(err);
    } finally {
      setLoading(false);
    }
  };

  return (
    <main className="entrie-create">
      <form onSubmit={handleSubmit}>
        {/*TENDRIAMOS que ponert otro textarea para agregar la URL??????? */}
        <h2> Titulo de la entrada </h2>
        <textarea
          value={text}
          onChange={(e) => setText(e.target.value)}
          autoFocus
          required
        />
        <button disabled={loading}>Enviar</button>
      </form>
    </main>
  );
};

export default EntrieCreate;
