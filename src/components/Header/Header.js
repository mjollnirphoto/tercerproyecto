import { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';
import { useToken } from '../../TokenContext';

import './Header.css';

const Header = () => {
  const [token, setToken] = useToken();
  const [user, setUser] = useState();
  useEffect(() => {
    const userData = async () => {
      try {
        const res = await fetch('http://localhost:4000/users', {
          headers: {
            Authorization: token,
          },
        });
        const body = await res.json();

        if (body.status === 'error') {
          alert(body.message);
        } else {
          setUser(body.data.user);
        }
      } catch (err) {
        console.error(err);
      }
    };

    if (token) userData();
  }, [token]);

  return (
    <header>
      <h1>
        <NavLink to="/"> Insert your URL </NavLink>
      </h1>
      <nav>
        {token && user && <p>@{user.name}</p>}
        {!token && (
          <div className="button">
            <NavLink to="/login">🔒Login</NavLink>
          </div>
        )}
        {!token && (
          <div className="button">
            <NavLink to="/register">Registro📝</NavLink>
          </div>
        )}
        {token && (
          <div className="button">
            <NavLink to="/message">Mensaje</NavLink>
          </div>
        )}
        {token && (
          <div className="button" onClick={() => setToken(null)}>
            <p>Cerrar Sesión</p>
          </div>
        )}
      </nav>
    </header>
  );
};

export default Header;
