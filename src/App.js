import './App.css';

import Header from './components/Header/Header';
import Register from './components/Register/Register';
import Login from './components/Login/Login';
import EntrieSearch from './components/EntrieSearch/EntrieSearch';
import EntrieCreate from './components/EntrieCreate/EntrieCreate';
import Footer from './components/Footer/Footer';
import { Routes, Route } from 'react-router-dom';

function App() {
  return (
    <div className="App">
      <Header />
      <Routes>
        <Route path="/" element={<EntrieSearch />} />
        <Route path="/register" element={<Register />} />
        <Route path="/login" element={<Login />} />
        <Route path="/message" element={<EntrieCreate />} />
        <Route path="*" element={<EntrieSearch />} />
      </Routes>
      <Footer />
    </div>
  );
}

export default App;
