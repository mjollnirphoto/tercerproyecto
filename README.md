1 - Desde la Terminal creamos un nuevo proyecto de React para ello utilizamos el comando :
`npx create-react-app cliente`
cliente es el nombre de la carpeta que vamos a crear y donde nos crerá todos los archivos.

2 - Tras unos 5 o 10 minutos, se nos crearan todos los archivos. Muchos son archivos que no queremos para nada,
podremos borrarlos para facilitarnos la comprensión de todo.
Cambiaremos lo necesario, las cosas del index.html necesarias tales como title, los metas, icono ...
( para poder ir viendo los cambios podemos ejecutar la terminar para ver todo en el navegador mediante el comando :
`npm start`
de esta forma iremos viendo que las modificaciones y los borrados no afectan de manera erronea )

3 - limpiamos el index.css y le aplicamos a todo el documento, para empezar de 0 el diseño.

- {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
  }

4 - vamos a app.js , limpiamos el fichero y dejaremos solamente, de esta manera sera la base de nuestro documento
import './App.css';

function App() {
return <div className="App"></div>;
}

export default App;

5 - vamos a la carpeta SRC y creamos una carpeta llamada /components/ en donde creamos en primer lugar la estructura
y creamos las carpetas /Header/ , /EntrieSearch/ y /Footer/ cada una contendra dos archivos, un css y un js con sus
respectivos nombres.

6 - en cada archivo js pondremos :
const Header = () => {
return (

<header>
<h1>Insert your URL</h1>
</header>
);
};

export default Header;
una estructura base que importamos en App.js y metemos dentro del DIV.

7 - si queremos diseñamos un poco el App.css para ir viendo visualmente como nuestros elementos cobran forma.

8 - ahora vamos a comenzar a trabajar con las partes correspondientes a las entradas ( entrie ) o lo que viene
siendo lo mismo, los post del usuario.
Nos dirigimos al archivo EntrieSeach.js y comenzamos a añadir las partes que controlaran las entradas.
Encima del return comenzamos a escribir codigo.
crearemos los useState y los useEffect y creamos una estructura de ul, li y la base de todas las entradas,
en este caso utilizamos una estructura de header, p, div, p y un footer.
Nos acordaremos de ir importando los css y por supuesto todo al App.js.

9 - despues de tener todo ordenado, volveremos al archivo EntrieSearch.js y creamos un boton que nos sirva
para buscar las entradas, mediante un form, input y button y añadiremos las opciones para que el boton no
cargue si esta en plena busqueda ....

10 - Despues de añadir todo lo necesiario a EntrieSeach necesitamos hacer la paginacion, o lo que es lo mismo
hacer las dependecias y necesitamos instalar el React Routes desde la terminal.

11 - Vamos a intalar la dependencia :
`npm i react-router-dom`

12 - nos vamos a index.js y vamos a importar lo siguiente :
`import {BrowserRouter} from 'react-router-dom'`
y acto seguido vamos a implementarlo encima de <App/>
de tal forma que nos queda de esta forma :
<BrowserRouter>
<App/>
</BrowserRouter>

13 - vamos a App.js e importamos : `import {Routes, Route} from 'react-router-dom`
y la añadimos debajo de Header
<Routes>
<Routes path='/' element={<EntrieSearch/>}/>
</Routes>

tambien añadiremos
<Route path='\*' element={<EntrieSearch />} />
de tal forma que si la ruta no es la correcta va a la raiz.

14 - vamos a crear los componentes de registro y login.
15 - creamos las carpetas Register y Login con sus ficheros js y css.
16 - escribimos codigo primero en Register.js para crear la estructura con los label, los input y el
boton de registro.
17 - podemos meter el css importado en Register.js y de esta forma veremos la creacion de los campos
name, email y password.
18 - vamos a Login.js , podemos copiar todo lo de Register.js porque la estructura va ser muy similar,
eliminaremos la parte del name, ya que para Login solamente vamos a pedir un email y un password.
No olvidarse de importarlo en App.js y de meterlo en la route cambiando los nombres necesarios.
